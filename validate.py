import sys
import json

def validate_data(children):
 
    previous=None
    done=False
    for child in children:
        if not previous:
            if child["start_offset_ms"] != 0 or child["start_offset_sec"] != 0:
                print("the chapters does not start at 0")
                return
            previous = child
            continue
        offset = previous["start_offset_ms"] + previous["length_ms"]
        if child["start_offset_ms"] != offset:
            print("Chapter ", child["title"], " has the wrong offset it is ", child["start_offset_ms"], " and should be ", offset)
            return
        previous = child
    print("the file was all ok!")

if len(sys.argv) != 2:
    print("usage: python " + sys.argv[0] + " filename")
    sys.exit(2)

data = json.load(open(sys.argv[1]));

validate_data(data["content_metadata"]["chapter_info"]["chapters"])




