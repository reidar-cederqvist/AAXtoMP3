import sys
import json

def flatten_chapters(y):
    out = []
 
    def flatten(x, level=0, parrent=None):
 
        done=False
        for a in x:
            if level>0:
                a["title"] = parrent["title"] + ' - ' + a["title"]
                if not done:
                    a["length_ms"] = parrent["length_ms"] + a["length_ms"]
                    a["start_offset_ms"] = parrent["start_offset_ms"]
                    a["start_offset_sec"] = parrent["start_offset_sec"]
                    done=True

            if 'chapters' in a:
                sub_chapters = a["chapters"] 
                del a["chapters"]
                flatten(sub_chapters, level+1, a)

            else:
                out.append(a)
            
    flatten(y)
    return out

if len(sys.argv) != 2:
    print("usage: python flatten_chapters.py filename")
    sys.exit(2)

data = json.load(open(sys.argv[1]));

data["content_metadata"]["chapter_info"]["chapters"] = flatten_chapters(data["content_metadata"]["chapter_info"]["chapters"])

print(json.dumps(data, indent=4, sort_keys=False));



